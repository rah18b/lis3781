
# LIS3781 Advanced Database Management

## Rachel Hester

### Project 1 Requirements:


#### README.md file should include the following items:

1. Screenshot of *your* ERD; 
2. Optional: SQL code for the required reports. 
3. Bitbucket repo links: *Your* lis3781 Bitbucket repo link 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
1. Screenshot of P1 ERD
![P1 ERD](img/P1_ERD.png)

2. Screenshot of Person Table
![Person Table](img/P1_PersonTable.png)


*Links to Assignment Files Below:* 

1. Link to SQL File
[SQL File](docs/lis3781_p1_solutions.sql "P1 SQL File")

2. Link to MWB File
[MWB File](docs/P1_ERD.mwb "P1 MWB File")


