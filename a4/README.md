
# LIS3781 Advanced Database Management

## Rachel Hester

### Assignment 4 Requirements:

#### README.md file should include the following items:

1. Screenshot of *your* ERD; 
2. Optional: SQL code for the required reports. 
3. Bitbucket repo links: *Your* lis3781 Bitbucket repo link 
 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

1. *Screenshot of ERD:*
![SQL Server ERD](img/LIS3781_A4_ERD.png)


*Link to Assignment Files Below:* 

1. Link to SQL File
[SQL File](docs/lis3781_a4_solutions.sql "A4 SQL File for Creation and Population of Tables")



