> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 - Advanced Database Management

## Rachel Hester

### LIS3781 Requirements:

*Course Work Links:*

1. [a1 README.md](a1/README.md "My a1 README.md file")
- README.md file should include the following items: 
    1. Screenshot of ampps installation running (#1 above); 
    2. git commands w/short descriptions (“Lesson 3b - Version Control Systems: Course Configuration”); 
    3. *Your* ERD image 
    4. Bitbucket repo links: 
        - a. This assignment, and  
        - b. The completed tutorial repo above (bitbucketstationlocations.  
        (See link in screenshot below.) 
  
   

2. [a2 README.md](a2/README.md "My a2 README.md file")
- README.md file should include the following items: 
    1. Screenshot of *your* SQL code; 
    2. Screenshot of *your* populated tables; 
 
  
	

3. [a3 README.md](a3/README.md "My a3 README.md file")
- README.md file should include the following items: 
    1. Screenshot of *your* SQL code used to create and populate your tables; 
    2. Screenshot of *your* populated tables (w/in the Oracle environment); 
    3. Optional: SQL code for a few of the required reports. 
    4. Bitbucket repo links: *Your* lis3781 Bitbucket repo link 
   
    

4. [a4 README.md](a4/README.md "My a4 README.md file")
- README.md file should include the following items: 
    1. Screenshot of *your* ERD; 
    2. Optional: SQL code for the required reports. 
    3. Bitbucket repo links: *Your* lis3781 Bitbucket repo link 
 
  


5. [a5 README.md](a5/README.md "My a5 README.md file")
- README.md file should include the following items: 
    1. Screenshot of *your* ERD; 
    2. Screenshot: At least *one* required report (i.e., exercise below), and SQL code solution. 
    3. Bitbucket repo links: *Your* lis3781 Bitbucket repo link 
   
  

6. [p1 README.md](p1/README.md "My p1 README.md file")
- README.md file should include the following items: 
    1. Screenshot of *your* ERD; 
    2. Optional: SQL code for the required reports. 
    3. Bitbucket repo links: *Your* lis3781 Bitbucket repo link 
   
    
7. [p2 README.md](p2/README.md "My p2 README.md file")
- README.md file should include the following items: 
    1. Screenshot of at least one MongoDB shell command(s), (e.g., show collections)
    2. Screenshot: At least *one* required report (i.e., exercise below), and JSON code solution.  
    3. Bitbucket repo links: *Your* lis3781 Bitbucket repo link 
   


