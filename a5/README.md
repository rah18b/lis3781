
# LIS3781 Advanced Database Management

## Rachel Hester

### Assignment 5 Requirements:

#### README.md file should include the following items:

1. Screenshot of *your* ERD; 
2. Screenshot: At least *one* required report (i.e., exercise below), and SQL code solution. 
3. Bitbucket repo links: *Your* lis3781 Bitbucket repo link 
 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
1. *Screenshot of ERD*
![ERD](img/LIS3781_A5_ERD.png)

2. *Screenshot of one Required Report*
![ERD](img/LIS3781_A5_Q1.png)


*Links to SQL Files Below:* 

1. Links to SQL Files
[SQL File](docs/lis3781_a5_solutions.sql "A5 SQL File")



