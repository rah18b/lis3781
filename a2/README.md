
# LIS3781 Advanced Database Management

## Rachel Hester

### Assignment 2 Requirements:


#### README.md file should include the following items:

1. Screenshot of *your* SQL code; 
2. Screenshot of *your* populated tables; 
 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of SQL Code 1*:             |  *Screenshot of SQL Code 2*:
:-------------------:|:------------------------------:
![SQL Code](img/sql_code_1.png)  |  ![SQL Code](img/sql_code_2.png)

*Screenshot of SQL Code 3*:             |  *Screenshot of Granted Privileges*:
:-------------------:|:------------------------------:
![SQL Code](img/sql_code_3.png)  |  ![Granted Privileges](img/grant_permissions.png)

*Screenshot of Populated Tables*
![Populated Tables](img/tables_populated.png)

*Screenshot of Local SQL Users*
![Local Users](img/mysql_users.png)

*Screenshot of User 1 - Insert Company Permissions*:             |  *Screenshot of User 1 - Insert Customer Permissions*:
:-------------------:|:------------------------------:
![User 1 - Insert on Company](img/user1_company_insert_permissions.png)  |  ![User 1 - Insert on Customer](img/user1_insert_permissions.png)

*Screenshot of User 2 - Select Permissions*:             |  *Screenshot of User 2 - Delete Permissions*:
:-------------------:|:------------------------------:
![User 2 - Select on Company](img/user2_select_permissions.png)  |  ![User 2 - Delete on Customer](img/user2_delete_permissions.png)

*Link to SQL Doc Below:* 

1. Link to SQL Doc 
[SQL File](docs/lis3781_a2_solutions.sql "A2 SQL Solutions")


