
# LIS3781 Advanced Database Management

## Rachel Hester

### Project 2 Requirements:

#### README.md file should include the following items:

1. Screenshot of at least one MongoDB shell command(s), (e.g., show collections); 
2. Screenshot: At least *one* required report (i.e., exercise below), and JSON code solution.  
3. Bitbucket repo links: *Your* lis3781 Bitbucket repo link 
 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
1. Screenshots of MongoDB

*Screenshot of MongoDB Shell Command*:             |  *Screenshot of One Required Report*:
:-------------------:|:------------------------------:
![P2 Shell Command](img/p2_shellcommand.png)  |  ![P2 Required Report](img/p2_requiredreport.png)


*Links to SQL Files Below:* 

1. Links to SQL Files
[SQL File](docs/lis3781_p2.sql "SQL File")



