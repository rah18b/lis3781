
# LIS3781 Advanced Database Management

## Rachel Hester

### Assignment 1 Requirements:


#### README.md file should include the following items:

1. Screenshot of ampps installation running (#1 above); 
2. git commands w/short descriptions (“Lesson 3b - Version Control Systems: Course Configuration”); 
3. *Your* ERD image 
4. Bitbucket repo links: 
    - a. This assignment, and  
    - b. The completed tutorial repo above (bitbucketstationlocations.  
    (See link in screenshot below.) 
    
> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Git commands w/short descriptions:

1. git init - creates new Git repository
2. git status - displays the state of the working directory and the staging area
3. git add - adds a change in the working directory to the staging area
4. git commit - used to save your changes to local repository
5. git push - used to upload local repository content to a remote repository
6. git pull - used to fetch and download content from a remote repository and immediately update the local repository to match that content
7. git uncommit - uncommit your last commit in git 

#### Assignment Screenshots:

1. *Screenshot of AMPPS (SQL on HomeBrew for Big Sur Mac):*
![AMPPS Installation Screenshot](img/fake_ampps.png)

2. *Screenshot of ERD*
![ERD Image](img/LIS3781_A1_ERD.png)

3. *Screenshot of Query 1*:
![Query 1](img/Query1.png)

4. *Screenshot of Query 2*:
![Query 2](img/Query2.png)

5. *Screenshot of Query 3*:  
![Query 3](img/Query3.png)

*Screenshot of Query 4*:             |  *Screenshot of Query 5*:
:-------------------:|:------------------------------:
![Query 5](img/Query4.png)  |  ![Query 6](img/Query5.png)

6. *Screenshot of Query 6*:
![Query 6](img/Query6.png)

7. *Screenshot of Query 7 part 1*: 
![Query 7 Part 1](img/query7_1.png)  

8. *Screenshot of Query 7 part 2*
![Query 7 Part 2](img/query7_2.png)


### Files:

1. Link to MWB File
[MWB File](docs/LIS3781_A1.mwb "A1 MWB")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rah18b/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

