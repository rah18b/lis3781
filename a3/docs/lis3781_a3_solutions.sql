SET DEFINE OFF

DROP SEQUENCE seq_cus_id;
Create sequence seq_cus_id
start with 1 
increment by 1
minvalue 1 
maxvalue 10000;

drop table customer CASCADE CONSTRAINTS PURGE;
CREATE TABLE customer
(
    cus_id number(3,0) not null,
    cus_fname varchar2(15) not null,
    cus_lname varchar2(30) not null,
    cus_street varchar2(30) not null,
    cus_city varchar2(30) not null,
    cus_state char(2) not null,
    cus_zip number(9) not null,
    cus_phone number(10) not null,
    cus_email varchar2(100),
    cus_balance number(7,2),
    cus_notes varchar2(255),
    CONSTRAINT pk_customer PRIMARY KEY(cus_id)
);

DROP SEQUENCE seq_com_id;
Create sequence seq_com_id
start with 1 
increment by 1 
minvalue 1 
maxvalue 10000;

drop table commodity CASCADE CONSTRAINTS PURGE;
CREATE TABLE commodity
(
    com_id number not null,
    com_name varchar2(20),
    com_price NUMBER(8,2) NOT NULL,
    cus_notes varchar2(255),
    CONSTRAINT pk_commodity PRIMARY KEY(com_id),
    CONSTRAINT uq_com_name UNIQUE(com_name)
);

DROP SEQUENCE seq_ord_id; -- for auto increment
Create sequence seq_ord_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table "order" CASCADE CONSTRAINTS PURGE;
Create table "order"
(
  ord_id number (4,0) not null,
  cus_id number,
  com_id number,
  ord_num_units number(5,0) not null,
  ord_total_cost number(8,2) not null,
  ord_notes varchar2(255),
  CONSTRAINT pk_order PRIMARY KEY(ord_id),
  CONSTRAINT fk_order_customer
  foreign key(cus_id)
  references customer(cus_id),
  CONSTRAINT fk_order_commodity
  foreign key(com_id)
  references commodity(com_id),
  CONSTRAINT check_unit CHECK(ord_num_units > 0),
  CONSTRAINT check_total CHECK(ord_total_cost > 0)
);

INSERT INTO customer VALUES (seq_cus_id.nextval, 'Beverly', 'Davis', '123 Main St.', 'Detroit', 'MI', 48252, 3135551212, 'bdavis@aol.com', 11500.99, 'recently moved');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Stephen', 'Taylor', '456 Elm St.', 'St. Louis', 'MO', 57252, 4185551212, 'staylor@comcast.net', 25.01, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Donna', 'Carter', '789 Peach Ave.', 'Los Angeles', 'CA', 48252, 3135551212, 'darter@wow.com',300.99, 'returning customer');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Robert', 'Silverman', '857 Wilbur Rd.', 'Phoenix', 'AZ', 25278, 4805551212, 'silverman@aol.com', NULL, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Sally', 'Victors', '534 Holler Way', 'Charleston', 'WV', 78345, 9045551212, 'svictors@wow.com', 500.76, 'new customer');
commit;

INSERT INTO commodity VALUES (seq_com_id.nextval, 'DVD & Player', 109.00, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Cereal', 3.00, 'sugar free');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Scrabble', 29.00, 'original');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Licorice', 1.89, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Tums', 2.45, 'antacid');
commit;

insert into "order" values (seq_ord_id.nextval, 2, 2, 12, 24, NULL);
insert into "order" values (seq_ord_id.nextval, 4, 3, 32, 160, NULL);
insert into "order" values (seq_ord_id.nextval, 5, 4, 3, 27, NULL);
insert into "order" values (seq_ord_id.nextval, 2, 1, 12, 36.00, NULL);
insert into "order" values (seq_ord_id.nextval, 1, 5, 4, 30.00, NULL);
commit;

select * from customer;
select * from commodity;
select * from "order";


