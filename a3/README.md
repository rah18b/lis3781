
# LIS3781 Advanced Database Management

## Rachel Hester

### Assignment 3 Requirements:


#### README.md file should include the following items:

1. Screenshot of *your* SQL code used to create and populate your tables; 
2. Screenshot of *your* populated tables (w/in the Oracle environment); 
3. Optional: SQL code for a few of the required reports. 
4. Bitbucket repo links: *Your* lis3781 Bitbucket repo link 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of SQL Code 1*:             |  *Screenshot of SQL Code 2*:
:-------------------:|:------------------------------:
![SQL Code](img/sql_code1.png)  |  ![SQL Code](img/sql_code2.png)

*Screenshot of SQL Code 3*
![SQL Code](img/sql_code3.png)

*Screenshot of Customer Table*:             |  *Screenshot of Commodity Table*:
:-------------------:|:------------------------------:
![Customer Table](img/customer_table.png)  |  ![Commodity Table](img/commodity_table.png)

*Screenshot of Order Table*
![Order Table](img/ord_table.png)


*Links to Assignment Files Below:* 

1. Link to SQL File
[SQL File](docs/lis3781_a3_solutions.sql "A3 SQL File")


